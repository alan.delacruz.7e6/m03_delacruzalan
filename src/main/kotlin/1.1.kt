/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.1 Hello World!
*/
fun main(args: Array<String>) {
    println("Hello World!")
}