/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.11 Calculadora de volum d’aire
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la llargada: ")
    val llargada = scanner.nextDouble()
    println("Introdueix la amplada: ")
    val amplada = scanner.nextDouble()
    println("Introdueix la alçada: ")
    val alzada = scanner.nextDouble()
    println("Volum d'aire que cap: ")
    print(llargada * amplada * alzada)
}