/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.12 De Celsius a Fahrenheit
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la temperatura: ")
    val temperatura = scanner.nextDouble()
    println("Aquest es la temperatura en Fahrenheit: ")
    val resultado = (temperatura * 1.8)
    print(resultado + 32)
}