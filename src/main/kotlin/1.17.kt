/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.17 És edat legal
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la teva edat: ")
    val edat = scanner.nextInt()
    print("Ets major?: ")
    print(edat >= 18)
}