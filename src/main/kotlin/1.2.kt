/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.2 Dobla l’enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt()
    println("Aquest és el número introduït: ")
    println(userInputValue *2)
}


