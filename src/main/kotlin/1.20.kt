/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.20 Nombres decimals iguals
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix número: ")
    val numero1 = scanner.nextDouble()
    val numero2= scanner.nextDouble()
    print("Són iguals?: ")
    print(numero1 == numero2)
}