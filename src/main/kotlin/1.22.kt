/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.22 Qui riu últim riu millor
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numero1 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero2 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero3 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero4 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero5 = scanner.nextInt()
    println("Introdueix un número: ")
    print("Són iguals?: ")
    print((numero5 > numero1) && (numero5 > numero2) && (numero5 > numero3) && (numero5 > numero4))
}