/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/22
* TITLE: 1.23 És una lletra?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un carácter: ")
    val caract1 = scanner.next().single()
    println("Correspon a una lletra?: ")
    print((caract1 >= 'A') && (caract1 <= 'Z') || (caract1 >= 'a') && (caract1 <= 'z'))
}