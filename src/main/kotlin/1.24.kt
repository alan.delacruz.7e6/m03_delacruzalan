/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/22
* TITLE: 1.24 És un nombre?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un carácter: ")
    val caract1 = scanner.next().single()
    println("Correspon a un nombre?: ")
    print((caract1 >= '0') && (caract1 <= '9'))
}