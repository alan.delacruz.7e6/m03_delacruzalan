/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/22
* TITLE: 1.26 Fes-me minúscula?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un carácter: ")
    val caract1 = scanner.next().single()
    println("Lletra en minúscula?: ")
    print(caract1.toLowerCase())
}