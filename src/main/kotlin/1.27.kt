/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/22
* TITLE: 1.27 Som iguals?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un carácter: ")
    val caract1 = scanner.next().single()
    println("Introdueix un altre carácter: ")
    val caract2 = scanner.next().single()
    println("Correspon a la mateixa lletra?: ")
    print (((caract1.toLowerCase()) == (caract2.toLowerCase())) && (((caract1.toUpperCase()) == (caract2.toUpperCase()))))
}