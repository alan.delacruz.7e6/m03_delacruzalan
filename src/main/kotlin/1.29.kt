/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.29 Equacions de segon grau
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numa = scanner.nextInt()
    println("Introdueix un número: ")
    val numb = scanner.nextInt()
    println("Introdueix un número: ")
    val numc = scanner.nextInt()

    println((-numb - (sqrt(numb.toDouble().pow(2) - 4 * numa * numc))) / (2 * numa))
    println((-numb + (sqrt(numb.toDouble().pow(2) - 4 * numa * numc))) / (2 * numa))


}