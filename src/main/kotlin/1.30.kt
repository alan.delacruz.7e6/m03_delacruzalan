/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.30 Quant de temps?
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numa = scanner.nextInt()
    val horas =  60
    val minutos = numa / 60
    val segundos = numa % 60

    print(minutos / horas)
    print(" hora ")
    print(minutos % 60)
    print(" minuts ")
    print(segundos)
    print(" segons")
}