/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.32 Calcula el capital (no LA capital)
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el capital: ")
    val capital = scanner.nextInt()
    println("Introdueix els anys: ")
    val anys = scanner.nextInt()
    println("Introdueix el tant per cent: ")
    val interessos = scanner.nextDouble()

    println((capital/100*interessos)*anys+capital)
}