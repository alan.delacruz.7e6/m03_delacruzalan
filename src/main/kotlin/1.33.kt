/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.33 És divisible?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numero1 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero2 = scanner.nextInt()

    println(numero2 % numero1 == 0)
}