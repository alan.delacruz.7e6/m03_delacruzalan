/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.35 Creador de targetes de treball
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom: ")
    val nom = scanner.next()
    println("Introdueix el teu cognom: ")
    val cognom = scanner.next()
    println("Introdueix el teu número de despatx: ")
    val num = scanner.nextInt()

    println("Empleada: $nom $cognom - Despatx: $num")
}