/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.1 Màxim de 3 nombres enters
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numero1 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero2 = scanner.nextInt()
    println("Introdueix un número: ")
    val numero3 = scanner.nextInt()

    if ((numero1 > numero2) && (numero1 > numero3)) {
        println(numero1)
    } else if ((numero2 > numero1) && (numero2 > numero3)) {
        println(numero2)
    } else println(numero3)
}