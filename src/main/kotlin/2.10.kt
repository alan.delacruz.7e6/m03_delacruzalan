/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/6
* TITLE: 2.10 Calcula la lletra del dni
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el dni: ")
    val dni = scanner.nextInt()
    val resultdni = dni%(dni/23)
    if(resultdni == 0) println("La teva lletra és: T")
    else if (resultdni == 1 ) {println("La teva lletra és R")}
    else if (resultdni == 2 ) {println("La teva lletra és W")}
    else if (resultdni == 3 ) {println("La teva lletra és A")}
    else if (resultdni == 4 ) {println("La teva lletra és G")}
    else if (resultdni == 5 ) {println("La teva lletra és M")}
    else if (resultdni == 6 ) {println("La teva lletra és Y")}
    else if (resultdni == 7 ) {println("La teva lletra és F")}
    else if (resultdni == 8 ) {println("La teva lletra és P")}
    else if (resultdni == 9 ) {println("La teva lletra és D")}
    else if (resultdni == 10 ) {println("La teva lletra és X")}
    else if (resultdni == 11 ) {println("La teva lletra és B")}
    else if (resultdni == 12 ) {println("La teva lletra és N")}
    else if (resultdni == 13 ) {println("La teva lletra és J")}
    else if (resultdni == 14 ) {println("La teva lletra és Z")}
    else if (resultdni == 15 ) {println("La teva lletra és S")}
    else if (resultdni == 16 ) {println("La teva lletra és Q")}
    else if (resultdni == 17 ) {println("La teva lletra és V")}
    else if (resultdni == 18 ) {println("La teva lletra és H")}
    else if (resultdni == 19 ) {println("La teva lletra és L")}
    else if (resultdni == 20 ) {println("La teva lletra és C")}
    else if (resultdni == 21 ) {println("La teva lletra és K")}
    else if (resultdni == 22 ) {println("La teva lletra és E")}
    else println(false)


}