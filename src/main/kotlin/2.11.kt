/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/06
* TITLE: 2.11 Calculadora
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val a = scanner.nextInt()
    println("Introdueix un altre número: ")
    val b = scanner.nextInt()
    println("Introdueix operador: ")
    val op = scanner.next()

    if (op == "+") println(a + b)
    else if (op == "-") println(a - b)
    else if (op == "*") println(a * b)
    else if (op == "/") println(a / b)
    else if (op == "%") println(a % b)
}