/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/06
* TITLE: 2.12 Comprova la data
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un dia: ")
    val dia = scanner.nextInt()
    println("Introdueix un mes: ")
    val mes = scanner.nextInt()
    println("Introdueix un altre any: ")
    val any = scanner.nextInt()

    when(mes){
        1,3,5,7,8,10,12 -> {
            if (dia in 1..31 && (any in 0..2022) && mes in 1..12)
                println("La data es correcte")
        }
        2 -> {
            if (dia in 1..28 && (any in 0..2022))
            println("La data es correcte")
            else(println("La data es incorrecte"))
        }

    }

}
//año bisiesto: (year%4 == 0) && (year%100 != 0 || year%400 == 0))