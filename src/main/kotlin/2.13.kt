/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.13 Quin dia de la setmana?
*/
import java.time.LocalDate
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un dia: ")
    val dia = scanner.nextInt()
    println("Introdueix un mes: ")
    val mes = scanner.nextInt()
    println("Introdueix un altre any: ")
    val any = scanner.nextInt()

    val day = LocalDate.of(any,mes,dia)

    println(day)


}

