/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.14 Any de traspàs
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix any: ")
    val year = scanner.nextInt()

    if ((year%4 == 0) && (year%100 != 0 || year%400 == 0) || year%100 != 0) println("Si és de traspàs")
    else println("No és de traspàs")
}
