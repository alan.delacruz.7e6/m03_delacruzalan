/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.15 És vocal o consonant?
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una lletra: ")
    val letra = scanner.next()

    when (letra){
        "a","e","i","o","u" -> println("És vocal")
        "A","E","I","O","U" -> println("És vocal")
        else -> println("És consonant")
    }
}
