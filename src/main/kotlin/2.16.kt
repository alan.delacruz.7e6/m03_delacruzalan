/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.16 Quants dies té el mes
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    val mesnum = scanner.nextInt()
    when(mesnum) {
        1,3,5,7,8,10,12 -> println(31)
        2 -> println("28/29")
        4,6,9,11 -> println(30)
        else -> println("Error")
    }

}