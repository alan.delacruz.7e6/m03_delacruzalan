/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.17 Valor absolut
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val numa = scanner.nextInt()
    println("Introdueix un numero: ")
    val numb = scanner.nextInt()
    println("Introdueix un numero: ")
    val numc = scanner.nextInt()

    if ((numa < numb) && (numa < numc) && (numb > numa) && (numb < numc) && (numc < numb) || (numc > numa)) println("ascendent")
    else if ((numa > numb) && (numb > numc)) (println("descendent"))
    else (println("cap de les dues"))
}