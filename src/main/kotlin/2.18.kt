/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.18 Valor absolut
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val numa = scanner.nextInt()

    println(abs(numa))

}