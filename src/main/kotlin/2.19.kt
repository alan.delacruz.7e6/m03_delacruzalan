/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.19 Quina nota he tret?
*/
import java.util.*
import kotlin.math.abs

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val nota = scanner.nextDouble()
    when (nota) {
        in 0.0..4.0 -> println("Insuficient")
        5.0 -> println("Suficient")
        6.0 -> println("Bé")
        in 7.0..8.0 -> println("Notable")
        in 9.0..10.0 -> println("Excel·lent")
        else -> println("Nota invàlida")
    }


}