/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.2 Salari
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix les hores: ")
    val numhores = scanner.nextInt()

    if (numhores == 40) println(numhores * 40)
    else if (numhores > 40) {println((numhores - 40) * 60 + 1600)}
}
