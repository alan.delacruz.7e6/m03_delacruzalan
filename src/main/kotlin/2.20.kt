/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/07
* TITLE: 2.20 Conversor d’unitats
*/
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un pes: ")
    val pes = scanner.nextDouble()
    println("Introudeix unitat: ")
    val unitat = scanner.next()

    if (unitat == "G") {
        println("kg: " + pes / 1000)
        println("t: " + pes / 1000000)
    }
    else if (unitat == "KG") {
        println("g: " + pes * 1000)
        println("t: " + pes / 1000)
    }
    else if (unitat == "T"){
        println("g: " + pes*1000000)
        println("kg: " + pes*1000)
    }
    else(println("desconegut"))
}

