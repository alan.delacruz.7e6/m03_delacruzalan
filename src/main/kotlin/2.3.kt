/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.3 És un bitllet vàlid
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el diner: ")
    val num = scanner.nextInt()

    if ((num == 10) || (num == 20) || (num == 50) || (num == 100) || (num == 500))println(true)
    else {
        println(false)
    }

}
