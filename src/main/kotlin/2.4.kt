/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.4 Té edat per treballar
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la teva edat: ")
    val num = scanner.nextInt()

    if (num >= 74)  (println("No té edat per treballar"))
    else if (num >= 16) (println("Té edat per treballar"))
    else (println("No té edat per treballar"))
}