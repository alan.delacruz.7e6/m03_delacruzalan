/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.5 En rang
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numa = scanner.nextInt()
    println("Introdueix un número: ")
    val numb = scanner.nextInt()
    println("Introdueix un número: ")
    val numc = scanner.nextInt()
    println("Introdueix un número: ")
    val numd = scanner.nextInt()
    println("Introdueix un número: ")
    val nume = scanner.nextInt()

    println(nume in numa..numb && nume in numc..numd)
    }
