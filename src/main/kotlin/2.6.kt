/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.6 Quina pizza és més gran?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val numa = scanner.nextInt()
    println("Introdueix un número: ")
    val numb = scanner.nextInt()
    println("Introdueix un número: ")
    val numc = scanner.nextInt()
    println("Introdueix un número: ")
    val numd = scanner.nextInt()
    println("Introdueix un número: ")
    val nume = scanner.nextInt()

    when (nume){
        in numa..numb -> println(true)
        in numc..numd -> println(true)
        else -> println(false)
    }
}