/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/06
* TITLE: 2.7 Parell o senar
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el numero: ")
    val num = scanner.nextInt()

    if (num % 2 == 0) {println("parell")}
    else {
        println("senar")
    }

}