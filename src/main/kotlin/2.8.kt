/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/29
* TITLE: 2.8 Afegeix un segon (2)
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)

    var hour = scanner.nextInt()
    var minutes = scanner.nextInt()
    var seconds = scanner.nextInt()

    if (++seconds == 60) {
        seconds = 0
        minutes += 1
    }
    if (minutes == 60) {
        minutes = 0
        hour += 1
    }
    if (hour == 24) hour = 0
    val secondsToPrint = if (seconds < 10) "0$seconds" else "$seconds"
    val minutesToPrint = if (minutes < 10) "0$minutes" else "$minutes"
    val hourToPrint = if (hour < 10) "0$hour" else "$hour"

    println("$hourToPrint:$minutesToPrint:$secondsToPrint")
}