/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/06
* TITLE: 2.9 Canvi mínim
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el euro: ")
    val euro = scanner.nextInt()
    println("Introdueix el cèntim: ")
    val cent = scanner.nextInt()

    if (cent > 99) println(false)
    else {
        val bit500 = euro / 500
        val bit200 = euro % 500 / 200
        val bit100 = euro % (500 / 200) / 100
        val bit50 = euro % 500 % 200 % 100 / 50
        val bit20 = euro % 500 % 200 % 100 % 50 / 20
        val bit10 = euro % 500 % 200 % 100 % 50 % 20 / 10
        val bit5 = euro % 500 % 200 % 100 % 50 % 20 % 10 / 5
        val mon2 = euro % 500 % 200 % 100 % 50 % 20 % 10 % 5 / 2
        val mon1 = euro % 500 % 200 % 100 % 50 % 20 % 10 % 5 % 2 / 1

        val cent50 = cent / 50
        val cent20 = cent % 50 / 20
        val cent10 = cent % 50 % 20 / 10
        val cent5 = cent % 50 % 20 % 10 / 5
        val cent2 = cent % 50 % 20 % 10 % 5 / 2
        val cent1 = cent % 50 % 20 % 10 % 5 % 2 / 1

        println("Bitllets de 500 euros: $bit500")
        println("Bitllets de 200 euros: $bit200")
        println("Bitllets de 100 euros: $bit100")
        println("Bitllets de 50 euros: $bit50")
        println("Bitllets de 20 euros: $bit20")
        println("Bitllets de 10 euros: $bit10")
        println("Bitllets de 5 euros: $bit5")
        println("Monedas de 2 euros: $mon2")
        println("Monedas de 1 euro: $mon1")
        println("Monedas de 50 cèntims: $cent50")
        println("Monedas de 20 cèntims: $cent20")
        println("Monedas de 10 cèntims: $cent10")
        println("Monedas de 5 cèntims: $cent5")
        println("Monedas de 2 cèntims: $cent2")
        println("Monedas de 1 cèntims: $cent1")
    }


}