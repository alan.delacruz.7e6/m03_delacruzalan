/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.1 Pinta X números
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()

    for (num in 1..num){
        println(num)
    }

}