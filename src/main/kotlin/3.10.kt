/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.10 Extrems
*/
/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.10 Extrems
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var entrega = scanner.nextInt()
    var mas = entrega
    var menor = entrega


    while (entrega != 0){
        entrega = scanner.nextInt()
        if (entrega < menor && entrega != 0) menor = entrega
        else if (entrega > mas && entrega != 0) mas = entrega
    }
    println(menor)
    println(mas)
}