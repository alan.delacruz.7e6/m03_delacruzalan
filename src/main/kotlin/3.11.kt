
/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.11 Suma de fraccions
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()
    var resultado = 1.0

    for (i in 2..num) {
        resultado += 1.0 / i


    }
    println(resultado)
}
