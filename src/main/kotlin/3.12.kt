/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.12 Divisible per 3 i per 5
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()


    for (i in 1..num){
        if   ((i % 3 == 0) && (i % 5 == 0)) println("$i és divisible per 3 i per 5")
        else if (i % 5 == 0) println("$i és divisible per 5")
        else if (i % 3 == 0) println("$i és divisible per 3")
        else("no es divisible")
    }


}