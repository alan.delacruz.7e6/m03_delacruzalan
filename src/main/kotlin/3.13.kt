/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.13 Logaritme natural de 2
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()
    var result = 0.0
    var result2 = 0.0
    for (i in 1..num step 2){
        result += 1.toDouble() / i
    }
    for (i in 2..num step 2){
        result2 += 1.toDouble() / i
    }

    println(result - result2)
}
