/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.14 Endevina el número
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var inputnum = null
    val aleatori = (1..100).random()

    while (inputnum != aleatori){
        var inputnum = scanner.nextInt()
        if (inputnum == aleatori) println("Has encertat!")
        else if (inputnum < aleatori) println("Massa baix")
        else if (inputnum > aleatori) println("Massa alt")
        else println("Incorrecte")
    }


}
