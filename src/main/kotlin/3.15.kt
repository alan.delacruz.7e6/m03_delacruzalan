/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.15 Endevina el número (ara amb intents)
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var inputnum = null
    val aleatori = (1..100).random()
    var intents = 0

    while (inputnum != aleatori || intents == 6){
        var inputnum = scanner.nextInt()

        if (inputnum == aleatori){
            println("Has encertat!")
            return
        }
        else if (inputnum < aleatori) {
            println("Massa baix")
            intents ++
        }
        else if (inputnum > aleatori) {
            println("Massa alt")
            intents ++
        }
        else{
            println("Incorrecte")
        }
        if (intents == 6){
            println("Has perdut")
            return
        }
    }


}