/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.16 Coordenades en moviment
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var posicio1 = 0
    var posicio2 = 0


    do {
        var lletra = scanner.next()
        when(lletra){
            "n" -> (posicio2 --)
            "s" -> (posicio2 ++)
            "e" -> (posicio1 ++)
            "o" -> (posicio1 --)
        }
    }
    while (lletra != "z")
    println("($posicio1,$posicio2)")
    }

