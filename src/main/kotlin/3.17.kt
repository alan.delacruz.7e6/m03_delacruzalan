/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.17 Factorial!
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var num = scanner.nextInt()
    var result = 1

    for (i in 1..num){
        result *= i
    }
    println(result)


}

