/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.2 Calcula la suma dels N primers
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()
    var resultado = 0

    for (i in 0..num){
        resultado = i + resultado
}
    println(resultado)

}