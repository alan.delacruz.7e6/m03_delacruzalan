/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.20 Triangle de nombres
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var num = scanner.nextInt()

    for (i in 1..num){
        println(" ")
        for (j in 1..i){
            print(j)
        }
    }



}