/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 3.22 Rombe d’*
*/
import java.lang.Math.abs
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()

    for (i in 0..2*num-1){
        val espais = abs(num-i)
        val asteriscos = if (2*i-1 < 2*num) 2*i-1
                        else (2*i-1) - (abs(num-i)*4)
        repeat(espais) {print(" ")}
        repeat(asteriscos)  {print("*")}
        println()
    }
}