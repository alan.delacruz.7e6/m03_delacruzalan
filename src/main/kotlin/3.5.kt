/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.5 Taula de multiplicar
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()

    for (i in 1..10) {
        val resultado = num * i
        println("$num x $i = $resultado")
    }


}