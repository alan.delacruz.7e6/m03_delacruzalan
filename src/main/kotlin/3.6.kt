/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.6 Eleva’l
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val base = scanner.nextInt()
    println("Introdueix un numero: ")
    val exponente = scanner.nextInt()
    var resultado = 1L

    for (i in 1 .. exponente) {
        resultado *= base
    }
    println(resultado)



}