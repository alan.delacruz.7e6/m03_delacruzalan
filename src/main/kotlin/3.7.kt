/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.7 Revers de l’enter
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var num = scanner.nextInt()
    var girado = 0

    while (num != 0) {
        val result = num % 10
        girado = girado * 10 + result
        num = num/10
    }
    println(girado)


}
