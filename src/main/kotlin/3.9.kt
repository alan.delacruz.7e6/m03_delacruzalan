/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/13
* TITLE: 3.9 És primer?
*/

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val checkprime = scanner.nextInt()
    println("Introdueix un numero: ")
    var numofdivisible = 0
    var i = 1
    while (i <= checkprime && numofdivisible <= 2) {
        if (checkprime%i == 0) numofdivisible++
        ++i
    }

    if (numofdivisible == 2 && i == checkprime) println("És primer")
    else println("No és primer")

}

