/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.14 Quants sumen...?
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val num = scanner.nextInt()

    for (i in 0..args.lastIndex) {
        for (j in i..args.lastIndex) {
            if (args[i].toInt() + args[j].toInt() == num) {
                println("${args[i]} ${args[j]}")
            }
        }
    }
}