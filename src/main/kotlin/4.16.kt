/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.16 Són iguals?
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix una paraula: ")
    var firstword = scanner.next()
    println("Introdueix altre paraula: ")
    var secondword = scanner.next()
    var check = true


    for (i in firstword.indices){
        if (firstword[i] != secondword[i]) check = false
    }
    if (check == true) println("Són iguals")
    else println("No son iguals")
}