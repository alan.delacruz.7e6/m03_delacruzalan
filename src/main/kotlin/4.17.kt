/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.16 Són iguals?
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix una paraula: ")
    var firstword = scanner.next().toLowerCase()
    println("Introdueix altre paraula: ")
    var secondword = scanner.next().toLowerCase()


    for (i in firstword.indices){
        if (firstword[i].toString() == secondword[i].toString())
            println("Son iguals")
        else println("No son iguals")
        break
    }
}