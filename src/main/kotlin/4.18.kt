/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.18 Purga de caràcters
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix una paraula: ")
    var firstword = scanner.next()
    

    do {
        println("Introdueix una lletra: ")
        val letra = scanner.next()
        firstword = firstword.replace(letra, "")

    }
    while (letra != "0")
        println(firstword)
}

