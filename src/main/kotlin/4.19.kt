/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.19 Substitueix el caràcter
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix una paraula: ")
    var firstword = scanner.next()
    println("Introdueix una lletra: ")
    val letra = scanner.next()
    val letra2 = scanner.next()
    firstword = firstword.replace(letra, letra2)
    println(firstword)

}

