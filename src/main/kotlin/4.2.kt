/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 4.2 Calcula la mitjana
*/
import java.util.*

fun main(args: Array<String>) {

    var resultado = 0

    for (arg in args){
        var numero = arg.toInt()
        resultado = numero + resultado
    }
    println(resultado.toDouble() / args.size)

}