/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.20 Distància d'Hamming
*/

import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix una sequence: ")
    var sequence = scanner.next()
    println("Introdueix una sequence: ")
    val sequence2 = scanner.next()
    var contador = 0

    if (sequence.length != sequence2.length) println("Entrada no válida")
    else {
        for (i in sequence.indices){
            if (sequence[i] != sequence2[i]){
                contador ++
            }
        }
    }
    println(contador)


}

