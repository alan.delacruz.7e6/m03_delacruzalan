/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.21 Parèntesis
*/

import java.util.*
fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    var parentesis = scanner.nextLine()
    var contador = 0

    for (i in parentesis.indices){
        if (contador < 0) {
            println("Están mal tancats")
            return
        }
        else if (parentesis[i] == '('){
            contador ++
        }
        else if (parentesis[i] == ')'){
            contador --
        }
    }
    if (contador == 0) println("Está bé tancats")
    else println("Están mal tancats")
}
