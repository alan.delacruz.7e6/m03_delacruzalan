/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 4.3 Calcula la lletra del dni
*/
import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu num del dni: ")
    var num = scanner.nextInt()
    val letra = arrayOf("T", "R", "W", "A", "G", "M","Y","F","P","D","X","B","N", "J","Z","S","Q","V","H","L","C","K","E")
    var dninum = num % 23
    print(num)
    print(letra[dninum])
}