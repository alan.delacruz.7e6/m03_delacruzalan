/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.31 Comptant a’s
*/

import java.util.*
fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    val frase = scanner.nextLine()
    var contador = 0

    for (i in frase.indices){
        if (frase[i] == 'a'){
            contador++
        }
        else if (frase[i] == '.'){
            break
        }
    }
    println(contador)
}