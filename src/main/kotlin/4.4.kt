/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 4.4 És contingut
*/
import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    var num = scanner.nextInt()
    val numeros = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    if (num in numeros) println("Si està contingut")
    else println("No està contingut")
}