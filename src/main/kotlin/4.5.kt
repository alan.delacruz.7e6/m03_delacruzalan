/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 4.5 En quina posició?
*/
import java.util.*

fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero: ")
    val num = scanner.nextInt()
    val numeros = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    println(numeros.indexOf(num))
}