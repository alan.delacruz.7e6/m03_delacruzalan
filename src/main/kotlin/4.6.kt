/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/14
* TITLE: 4.6 Mínim i màxim
*/

import java.util.*

fun main(args: Array<String>) {

    var min = args[0].toInt()
    var max = args[0].toInt()

    for (arg in args){
        val num = arg.toInt()
        if(num < min) min = num
        else if (num > max) max = num

    }

    println("min: $min max: $max")


}


