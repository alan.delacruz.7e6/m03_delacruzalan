/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.8 Igual a l'últim
*/

import java.util.*
fun main(args: Array<String>) {

    var cantidad = args.size
    var ultimo = args[cantidad - 1]
    var result = 0

    for (i in cantidad-2 downTo 1){
        if(args[i] == ultimo){
            result += 1
        }
    }
    println("Aquest és el últim número : $ultimo")
    println("Aquest son les vegades repetit : $result")


}

