/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/10/27
* TITLE: 4.9 Valors repetits
*/

import java.util.*
fun main(args: Array<String>) {

    for (i in 0..args.lastIndex) { // empieza por la posicion 0
        for (j in i+1..args.lastIndex){ //empieza por la siguiente posicion de i (0)
            if (args[i] == args[j]) println(args[i]) // va comparando si i es igual a j
        }
    }
}