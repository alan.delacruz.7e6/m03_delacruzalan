import java.util.*

fun main() {

    var resultat = 0



    for (i in 0..100 step 2) {
        if (i % 2 == 0) resultat += i
        else resultat -= i
    }
    println(resultat)
}