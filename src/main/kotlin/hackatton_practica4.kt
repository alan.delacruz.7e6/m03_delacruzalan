import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    var num = scanner.nextInt()
    var result = 1

    for (i in 1..num){
        if (i % 3 == 0)
            result *= i
    }
    println(result)
}