import java.util.*

fun main(args: Array<String>) {

    var resultado = 1

    for (arg in args){
       var numero = arg.toInt()
        if (numero >= 0)
        resultado *= numero
        else (numero * -1) * resultado
    }
    println(resultado)

}
